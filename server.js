const express = require('express')
const http = require('http')
const socketIO = require('socket.io')

// our localhost port
const port = 4001

const app = express()

// our server instance
const server = http.createServer(app)

// This creates our socket using the instance of the server
const io = socketIO(server)

// This is what the socket.io syntax is like, we will work this later
let NAME = '';
io.on('connection', socket => {
    console.log('New client connected', socket.id)
//    console.log(socket.handshake.address.address);
//    console.log(socket.handshake.url);
    socket.join('some room');
    // just like on the client side, we have a socket.on method that takes a callback function
    socket.on('change color', (color) => {
        // once we get a 'change color' event from one of our clients, we will send it to the rest of the clients
        // we make use of the socket.emit method again with the argument given to use from the callback function above
        console.log('Color Changed to: ', color)
        io.sockets.emit('change color', color)
    })

    socket.on('openVote', (groupName) => {
        // once we get a 'change color' event from one of our clients, we will send it to the rest of the clients
        // we make use of the socket.emit method again with the argument given to use from the callback function above
        io.sockets.emit('groupName', groupName);
    });

    socket.on('closeVote', (groupName) => {
        // once we get a 'change color' event from one of our clients, we will send it to the rest of the clients
        // we make use of the socket.emit method again with the argument given to use from the callback function above
        io.sockets.emit('groupName', groupName);
    });

    socket.once('open', () => {
        // once we get a 'change color' event from one of our clients, we will send it to the rest of the clients
        // we make use of the socket.emit method again with the argument given to use from the callback function above
        console.log("BERAT", NAME);
        console.log(NAME);
        io.sockets.emit('open', NAME)
    })

    // disconnect is fired when a client leaves the server
    socket.on('disconnect', () => {
        console.log('user disconnected', socket.id)
    })
})

server.listen(port, () => {
    console.log(`Listening on port ${port}`)
})